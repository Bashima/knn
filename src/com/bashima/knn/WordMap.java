package com.bashima.knn;

/**
 * @author bashimaislam
 *
 */
public class WordMap {
	String word;
	int count;
	
	public WordMap(String word) {
		super();
		this.word = word;
		this.count = 1;
	}
	public WordMap(String word, int co) {
		super();
		this.word = word;
		this.count = co;
	}
	public String getWord() {
		return word;
	}
	public void setWord(String word) {
		this.word = word;
	}
	public double getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public void increaseCount()
	{
		count++;
	}
	
}
