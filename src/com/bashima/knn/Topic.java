package com.bashima.knn;

import java.util.ArrayList;

public class Topic {
	public String topic;
	public ArrayList<WordMap> wordMap;
	public ArrayList<String> justWord;
	public int totalWord;
	public double probability;
	
	public Topic()
	{
		topic="";
		wordMap = new ArrayList<WordMap>();
		justWord = new ArrayList<String>();
		totalWord = 0;
		probability = 0.0;
	}
	
	public void calculateProb(ArrayList<Document> train)
	{
		int a =0;
		for (Document document : train) {

			if(document.topic.equals(topic))
			{
				a++;
			}
		}
		probability = a*1.0/train.size();
	}
}
