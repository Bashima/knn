package com.bashima.knn;

import java.util.ArrayList;

public class Document {
	public String name;
	public String topic;
	public ArrayList<WordMap> words;
	public ArrayList<String> justWord;
	public int totalWords;
	public ArrayList<Double> TF;
	public ArrayList<Double> d;
	public double dAbs;
	
	public Document() {
		name = "";
		topic = "";
		words = new ArrayList<WordMap>();
		justWord = new ArrayList<String>();
		totalWords = 0;
		TF = new ArrayList<Double>();
		d = new ArrayList<Double>();
		dAbs =0.0;
	}
	
	public void calculateTF()
	{
		for (WordMap wm : words) {
			double val = (wm.count*1.0 / totalWords*1.0);
			TF.add(val);
//			System.out.println(wm.word+ " "+wm.count+" "+ val);
		}
	}
	
	public void calculateD(ArrayList<WordMap> IDF, ArrayList<String> wordIDF)
	{
		calculateTF();
		double tempVal = 0.0;
		for (WordMap wm : words) {
			double idf = 0.0;
			if(wordIDF.contains(wm.word))
			{
			int index = wordIDF.indexOf(wm.word);
			idf = IDF.get(index).count;
			}
			double dTemp = TF.get(words.indexOf(wm))* idf;
			tempVal = tempVal + (dTemp*dTemp);
			d.add(dTemp);
//			System.out.println(wm.word + " "+ dTemp);
		}
		dAbs = Math.sqrt(tempVal);
	}
	
}


