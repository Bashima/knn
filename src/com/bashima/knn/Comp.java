package com.bashima.knn;

import java.util.Comparator;

public class Comp implements Comparator<Value>{
	public int compare(Value o1, Value o2)
	{
		return (o1.value-o2.value);
	}
}
