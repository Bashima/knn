package com.bashima.knn;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class KNN {
	public ArrayList<String> topic;
	public ArrayList<Topic> topicList;
	public ArrayList<String> topicName;
	public ArrayList<Document> trainDocument;
	public ArrayList<TestDocuments> testDocument;
	public ArrayList<WordMap> IDF;
	public ArrayList<String> wordsIDF;
	
	public double smoothingFactor = 2;
	public Set<String> wordSet = new HashSet<String>();

	public KNN() {
		topic = new ArrayList<String>();
		topicList = new ArrayList<Topic>();
		trainDocument = new ArrayList<Document>();
		testDocument = new ArrayList<TestDocuments>();
		IDF = new ArrayList<WordMap>();
		wordsIDF = new ArrayList<String>();
		topicName = new ArrayList<String>();
	}

	public void totalTrain() {
		for (Document train : trainDocument) {
			for (String word : train.justWord) {
				wordSet.add(word);
			}
		}
	}

	public void topicCalculate() {
		for (Document train : trainDocument) {
			if (topicName.contains(train.topic)) {
				int index = topicName.indexOf(train.topic);
				Topic tempTopic = topicList.get(index);
				for (WordMap words : train.words) {
					if (tempTopic.justWord.contains(words.word)) {
						int index1 = tempTopic.justWord.indexOf(words.word);
						WordMap tempMap = tempTopic.wordMap.get(index1);
						tempMap.count = tempMap.count + words.count;
						tempTopic.totalWord += words.count;
						tempTopic.wordMap.set(index1, tempMap);
					} else {
						WordMap w = new WordMap(words.word, words.count);
						tempTopic.justWord.add(w.word);
						tempTopic.wordMap.add(w);
						tempTopic.totalWord += words.count;
					}
				}
				topicList.set(index, tempTopic);
			} else {
				topicName.add(train.topic);
				Topic tempTopic = new Topic();
				tempTopic.topic = train.topic;
				for (WordMap words : train.words) {
					WordMap w = new WordMap(words.word, words.count);
					tempTopic.justWord.add(w.word);
					tempTopic.wordMap.add(w);
					tempTopic.totalWord += words.count;
				}
				topicList.add(tempTopic);
			}
		}

//		for (Topic top : topicList) {
//			System.out.println(top.topic);
//			for (WordMap wm : top.wordMap) {
//				System.out.println(wm.word + " " + wm.count);
//			}
//			System.out.println(top.totalWord);
//			System.out.println();
//		}
	}

	public void calProb() {
		for (Topic top : topicList) {
			top.calculateProb(trainDocument);
		}
	}

	public void naiveBiasian() {
		calProb();
		totalTrain();
		for (TestDocuments test : testDocument) {

			double maxV = -Double.MAX_VALUE;
			String finalTop = "";
			for (Topic top : topicList) {

				double val = Math.log(top.probability);
				for (String w : test.documents.justWord) {
					double x1 = 0.0;
					if (top.justWord.contains(w)) {
						int indexTOp = top.justWord.indexOf(w);
						int countinTop = top.wordMap.get(indexTOp).count;
						x1 = ((smoothingFactor + countinTop) * 1.0)
								/ (1.0 * (top.totalWord + wordSet.size()*smoothingFactor));
					} else {
						x1 = ((smoothingFactor) * 1.0)
								/ (1.0 * (top.totalWord + wordSet.size()*smoothingFactor));
					}
					val += Math.log(x1);
					System.out.println(val+ " "+ top.totalWord+" "+wordSet.size()+ " "+x1);
				}
				if (val > maxV) {
					maxV = val;
					finalTop = top.topic;
				}

			}
			test.calculatedTopic = finalTop;
		}
	}

	public void hamming(int i) {
		for (TestDocuments test : testDocument) {
			ArrayList<Value> valueList = new ArrayList<Value>();
			for (Document train : trainDocument) {
				int hammingDistance = 0;
				for (String word : test.documents.justWord) {
					if (train.justWord.contains(word)) {
					} else {
						hammingDistance++;
					}
				}
				for (String wordtrain : train.justWord) {
					if (test.documents.justWord.contains(wordtrain)) {
					} else {
						hammingDistance++;
					}
				}
				Value tempValue = new Value();
				tempValue.topic = train.topic;
				tempValue.value = hammingDistance;
				valueList.add(tempValue);
			}

			Collections.sort(valueList, new Comp());
			ArrayList<String> top = new ArrayList<String>();
			for (int j = 0; j < i; j++) {
				top.add(valueList.get(j).topic);
			}
			int c[] = new int[i];
			int max = -100;
			int ind = 0;
			for (int k = 0; k < i; k++) {
				c[k] = Collections.frequency(top, top.get(k));
				if (c[k] > max) {
					max = c[k];
					ind = k;
				}
			}

			test.calculatedTopic = top.get(ind);
			System.out.println(test.calculatedTopic);
		}
	}

	public void euclidian(int i) {
		for (TestDocuments test : testDocument) {
			ArrayList<Value> valueList = new ArrayList<Value>();
			for (Document train : trainDocument) {
				double euclidianDistance = 0.0;
				for (WordMap word : test.documents.words) {
					double trainCount = 0.0;
					if (train.justWord.contains(word.word)) {
						int index = train.justWord.indexOf(word.word);
						trainCount = train.words.get(index).count;
					}
					double minus = trainCount - word.count;
					double tempDistance = minus * minus;
					euclidianDistance = euclidianDistance + tempDistance;
				}
				for (WordMap wordTrain : train.words) {
					int trainCount = 0;
					if (test.documents.justWord.contains(wordTrain.word)) {
					} else {
						double minus = trainCount - wordTrain.count;
						double tempDistance = minus * minus;
						euclidianDistance = euclidianDistance + tempDistance;
					}

				}
				euclidianDistance = Math.sqrt(euclidianDistance);
				Value tempValue = new Value();
				tempValue.topic = train.topic;
				tempValue.value = (int) (euclidianDistance * 1000);
				valueList.add(tempValue);
			}
			Collections.sort(valueList, new Comp());
			ArrayList<String> top = new ArrayList<String>();
			for (int j = 0; j < i; j++) {
				top.add(valueList.get(j).topic);
			}
			int c[] = new int[i];
			int max = -100;
			int ind = 0;
			for (int k = 0; k < i; k++) {
				c[k] = Collections.frequency(top, top.get(k));
				if (c[k] > max) {
					max = c[k];
					ind = k;
				}
			}

			test.calculatedTopic = top.get(ind);
			System.out.println(test.calculatedTopic + " "
					+ test.documents.topic + " " + valueList.get(0).value);
		}
	}

	public void countIDF() {
		for (Document doc : trainDocument) {
			for (String word : doc.justWord) {
				if (wordsIDF.contains(word)) {
					int index = wordsIDF.indexOf(word);
					int newCount = IDF.get(index).count + 1;
					WordMap tempMap = new WordMap(word, newCount);
					IDF.set(index, tempMap);
				} else {
					WordMap w = new WordMap(word);
					wordsIDF.add(word);
					IDF.add(w);
				}
			}
		}
		for (WordMap wm : IDF) {

			wm.count = (int) (Math.log(trainDocument.size() * 1.0 / wm.count
					* 1.0) * 1000);
			// System.out.println(wm.word+ " "+ wm.count);
		}

	}
	public int initial =2;
	public void calculateD() {
		for (TestDocuments test : testDocument) {
			test.documents.calculateD(IDF, wordsIDF);
		}
		for (Document train : trainDocument) {
			train.calculateD(IDF, wordsIDF);
		}
	}

	public double dotProduct(TestDocuments test, Document train) {
		double value = 0;
		for (String word : test.documents.justWord) {
			int index = test.documents.justWord.indexOf(word);
			double v1 = test.documents.d.get(index);
			double v2 = 0;
			if (train.justWord.contains(word)) {
				int index2 = test.documents.justWord.indexOf(word);
				v2 = test.documents.d.get(index2);
			}
			value = value + (v1 * v2);
		}
		// System.out.println(value);
		return value;
	}

	public void cosineSimilarity(int i) {
		countIDF();
		calculateD();
		System.out.println("start");
		for (TestDocuments test : testDocument) {
			ArrayList<Value> valueList = new ArrayList<Value>();
			for (Document train : trainDocument) {
				double val = dotProduct(test, train);
				double cos = val / (test.documents.dAbs * train.dAbs);
				Value tempVal = new Value();
				tempVal.topic = train.topic;
				tempVal.value = (int) (cos * 1000000);
				valueList.add(tempVal);
			}
			// for (Value value : valueList) {
			// System.out.println(value.topic+" "+ value.value);
			// }
			System.out.println();
			Collections.sort(valueList, new Comp());
			// for (Value value : valueList) {
			// System.out.println(value.topic+" "+ value.value);
			// }
			// System.out.println();
			// break;
			ArrayList<String> top = new ArrayList<String>();
			for (int j = 1; j < i + 1; j++) {
				top.add(valueList.get(valueList.size() - j).topic);
				// System.out.println(valueList.get(valueList.size()-j).topic);
			}
			int max = -100;
			int ind = 0;
			for (int k = 0; k < i; k++) {
				int x = Collections.frequency(top, top.get(k));
				if (x > max) {
					max = x;
					ind = k;
				}
			}

			test.calculatedTopic = top.get(ind);
			System.out.println(test.calculatedTopic + " "
					+ test.documents.topic + " "
					+ valueList.get(valueList.size() - 1).value);
		}
	}

	public double accuracy() {
		int correct = initial;
		for (TestDocuments test : testDocument) {
			if (test.documents.topic.equals(test.calculatedTopic)) {
				correct++;
			}
		}
		double accuracy = correct * 100.0 / testDocument.size();
		return accuracy;

	}

	public static void main(String[] args) {
		ReadFile read = new ReadFile();
		KNN kn = new KNN();
		try {
			kn.topic = read.readTopic("topics.data");
		} catch (FileNotFoundException e) {
			System.out.println("Unable to open topics.data");
		}
		// System.out.println(kn.topic.toString());
		try {
			kn.trainDocument = read.readDocument("training.data");
		} catch (FileNotFoundException e) {
			System.out.println("Unable to open training.data");
		}

		try {
			kn.testDocument = read.readTestDocument("test.data");
		} catch (FileNotFoundException e) {
			System.out.println("Unable to open test.data");
		}
		// kn.hamming(1);
		// kn.euclidian(5);
		// kn.cosineSimilarity(5);
		// System.out.println(kn.accuracy());
		kn.topicCalculate();
		kn.naiveBiasian();
		System.out.println(kn.accuracy());
	}

}
