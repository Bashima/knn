package com.bashima.knn;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class ReadFile {

	public List<String> ignoreWords = new ArrayList<String>();

	public ReadFile() {
		ignoreWords = Arrays.asList("", "'", "a", "an", "the", "is", "to",
				"on", "in", "if", "at", "of", "by", "as", "are", "for", "but",
				"not", "be", "this", "was", "will", "that", "been", "have",
				"and", "with", "were", "after", "about");
	}

	public ArrayList<String> readTopic(String fileName)
			throws FileNotFoundException {
		ArrayList<String> tempTopic = new ArrayList<String>();
		Scanner scn = new Scanner(new File(fileName));
		while (scn.hasNext()) {
			String temp = scn.nextLine();
			tempTopic.add(temp);
		}
		scn.close();
		return tempTopic;
	}

	public ArrayList<TestDocuments> readTestDocument(String fileName)
			throws FileNotFoundException {
		ArrayList<TestDocuments> tempDocuments = new ArrayList<TestDocuments>();
		Scanner scn = new Scanner(new File(fileName));
		while (scn.hasNext()) {
			Document tempDocument = new Document();
			Boolean flagDoc = true;
			tempDocument.topic = scn.nextLine();
			scn.nextLine();
			scn.nextLine();
			scn.nextLine();
			scn.nextLine();
			scn.nextLine();
			// System.out.println("topic: " + tempDocument.topic);
			// String temp = scn.next();
			// System.out.println(temp);
			while (scn.hasNext() && flagDoc) {
				String temp = scn.nextLine();
				if (temp.equals("")) {
					String newTemp = scn.nextLine();
					if (newTemp.equals("")) {
						flagDoc = false;
					} else {
						temp = newTemp;
					}
				}
				String[] tempWordsArray = temp.replace(".", "")
						.replace(",", "").replace("-", "").replace(";", "")
						.replace("\"", "").replace("/", "").replace("[", "")
						.replace("]", "").replace("(", "").replace(")", "")
						.replace("{", "").replace("}", "").replace("+", "")
						.replace("=", "").replace("_", "").replace(":", "")
						.replace("<", "").replace(">", "").replace("?", "")
						.replace("0", "").replace("1", "").replace("2", "")
						.replace("3", "").replace("4", "").replace("5", "")
						.replace("6", "").replace("7", "").replace("8", "")
						.replace("9", "").split(" ");
				for (int i = 0; i < tempWordsArray.length; i++) {
					// System.out.println(tempWordsArray[i]);
					if (ignoreWords.contains(tempWordsArray[i])) {
					} else {
						tempDocument.totalWords++;
						if (tempDocument.justWord.contains(tempWordsArray[i])) {
							int index = tempDocument.justWord
									.indexOf(tempWordsArray[i]);
							int newCount = tempDocument.words.get(index).count + 1;
							WordMap tempMap = new WordMap(tempWordsArray[i],
									newCount);
							tempDocument.words.set(index, tempMap);
						} else {
							WordMap w = new WordMap(tempWordsArray[i]);
							tempDocument.justWord.add(tempWordsArray[i]);
							tempDocument.words.add(w);
						}
					}
				}
			}
			// System.out.println("words: " + tempDocument.words.size());
			TestDocuments tempTestDocument = new TestDocuments();
			tempTestDocument.documents = tempDocument;
			tempDocuments.add(tempTestDocument);
		}

		scn.close();
		// System.err.println(tempDocuments.size());
		return tempDocuments;

	}

	public ArrayList<Document> readDocument(String fileName)
			throws FileNotFoundException {
		ArrayList<Document> tempDocuments = new ArrayList<Document>();
		Scanner scn = new Scanner(new File(fileName));
		while (scn.hasNext()) {
			Document tempDocument = new Document();
			Boolean flagDoc = true;
			tempDocument.topic = scn.nextLine();
			scn.nextLine();
			scn.nextLine();
			scn.nextLine();
			scn.nextLine();
			scn.nextLine();
			// System.out.println("topic: " + tempDocument.topic);
			// String temp = scn.next();
			// System.out.println(temp);
			while (scn.hasNext() && flagDoc) {
				String temp = scn.nextLine();
				if (temp.equals("")) {
					String newTemp = scn.nextLine();
					if (newTemp.equals("")) {
						flagDoc = false;
					} else {
						temp = newTemp;
					}
				}
				String[] tempWordsArray = temp.replace(".", "")
						.replace(",", "").replace("-", "").replace(";", "")
						.replace("\"", "").replace("/", "").replace("[", "")
						.replace("]", "").replace("(", "").replace(")", "")
						.replace("{", "").replace("}", "").replace("+", "")
						.replace("=", "").replace("_", "").replace(":", "")
						.replace("<", "").replace(">", "").replace("?", "")
						.replace("0", "").replace("1", "").replace("2", "")
						.replace("3", "").replace("4", "").replace("5", "")
						.replace("6", "").replace("7", "").replace("8", "")
						.replace("9", "").split(" ");
				for (int i = 0; i < tempWordsArray.length; i++) {
					// System.out.println(tempWordsArray[i]);
					if (ignoreWords.contains(tempWordsArray[i])) {
					} else {
						tempDocument.totalWords++;
						if (tempDocument.justWord.contains(tempWordsArray[i])) {
							int index = tempDocument.justWord
									.indexOf(tempWordsArray[i]);
							int newCount = tempDocument.words.get(index).count + 1;
							WordMap tempMap = new WordMap(tempWordsArray[i],
									newCount);
							tempDocument.words.set(index, tempMap);
						} else {
							WordMap w = new WordMap(tempWordsArray[i]);
							tempDocument.justWord.add(tempWordsArray[i]);
							tempDocument.words.add(w);
						}
					}
				}
			}
			// System.out.println("words: " + tempDocument.words.size());
			tempDocuments.add(tempDocument);
		}

		scn.close();
		// System.err.println(tempDocuments.size());
		return tempDocuments;

	}
}
